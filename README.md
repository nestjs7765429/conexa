# Conexa Users and Films

## Instalación y ejecución

Asegurate de tener [Node.js](https://nodejs.org) y [npm](https://www.npmjs.com) instalado. El proyecto fue realizado con Node 16.

1. Abrí tu consola, posicionate en el directorio donde quieras alojar la aplicación.
2. Clona este repositorio con `git clone ${url-del-proyecto}` 
3. Entra a la carpeta conexa con `cd conexa`
4. Ejecuta `npm install` para instalar las dependencias.
5. Para levantar la aplicación ejecuta `npm run start` o `nest start`, es lo mismo.

## Configuración

Al inicializar, automáticamente se crea la base de datos en memoria y se popula con la información
necesaria para funcionar, un usuario admin, y los 2 roles que existen para manejar la autorización.
Los nombres de los roles persistidos en la base son tomados de la clase `RoleEnum`.
Los datos del usuario admin como la url del servicio SWAPI deben estar parametrizados en el `.env`

Crear el archivo .env en el root directory del proyecto con las siguientes variables definidas:

ADMIN_USER=admin 
ADMIN_PASSWORD=1234 
SWAPI_BASE_URL=https://swapi.dev/api/films 

Se deja en el repositorio el .env.example de ejemplo.

## Uso

Luego de haber ejecutado el paso 5 de la `instalación y ejecución`, se disponibilizará la documentación en la ruta:
- http://localhost:3000/api 

En esta ruta vas a poder hacer uso de la api y todos sus enpoints segun las consignas del challenge.

Además, dentro del proyecto, hay una carpeta `Postman` con una collection y un environment que se pueden importar también para su uso.

## Testing

Para correr los tests de la aplicación ejecutar `npm test`.


## Estructura de Directorios

la aplicación consta de 4 módulos principales:

- auth: Maneja el signin y el signup de usuarios.
- user: Contiene el ABM básico de usuarios.
- film: Contiene el ABM básico de películas.
- client: Es módulo de clientes HTTP. 

## Dependencias

Pricipales dependencias del proyecto:

- "@nestjs/core": "^10.0.0"  --> framework
- "@nestjs/swagger": "^7.1.1", --> OpenAPI
- "@nestjs/typeorm": "^10.0.0", --> ORM de base de datos
- "@nestjs/jwt": "^10.1.0", --> libreria de manejo de JWT
- "sqlite3": "^5.1.6", --> base de datos en memoria.
- "@nestjs/axios": "^3.0.0", --> Http requests.

## Autor

.- Dario Pepper!

# Challenge
## Objetivo

Construir una backend que tome información de la API pública de Star Wars y que sea utilizada en pos de crear una nueva aplicación de gestión de películas y series. El backend deberá estar desarrollado en Node.js usando Express o Nest.js.

## Ejercicio Práctico: Aplicación de Gestión de Películas

### Descripción del proyecto:

Tu objetivo es desarrollar una aplicación backend utilizando NestJS que permita gestionar películas. La aplicación debe cumplir con los siguientes requisitos:

Autenticación y autorización: Implementa un sistema de autenticación y autorización que permita a los usuarios registrarse, iniciar sesión y obtener un token de acceso. Utiliza JWT (JSON Web Tokens) para la autenticación.

Gestión de usuarios: Implementa los endpoints necesarios para el registro (sign-up) y login de usuarios. Al registrar un nuevo usuario, asegúrate de almacenar su información en una base de datos y de aplicar las validaciones necesarias.

Manejo de roles: Define dos roles de usuario: "Usuario Regular" y "Administrador". Los usuarios registrados por defecto serán "Usuarios Regulares". Solo los usuarios con el rol de "Administrador" deben tener acceso a las operaciones de creación, actualización y eliminación de películas.

Endpoints de la API:
- Endpoint para registro de nuevos usuarios.
- Endpoint para registro de nuevos usuarios.
- Endpoint para obtener la lista de películas.
- Endpoint para obtener los detalles de una película específica. Solo los "Usuarios Regulares" deberían tener acceso a este endpoint.
- Endpoint para crear una nueva película. Solo los "Administradores" deberían tener acceso a este endpoint.
- Endpoint para actualizar la información de una película existente. Solo los "Administradores" deberían tener acceso a este endpoint.
- Endpoint para eliminar una película. Solo los "Administradores" deberían tener acceso a este endpoint.

Pruebas unitarias: Escribe pruebas unitarias para verificar el correcto funcionamiento de los endpoints, la lógica de negocio de la aplicación y la restricción de acceso basada en roles.

Recursos:

- Puedes utilizar la documentación oficial de NestJS (https://docs.nestjs.com/) como referencia durante el desarrollo.
- Utiliza el control de versiones Git para gestionar el código fuente de la aplicación.

Entregables:

- Repositorio de Git que contenga el código fuente de la aplicación.
- Instrucciones claras sobre cómo ejecutar y probar la aplicación localmente.

Este test va a estar evaluado de la siguiente manera:

- Diseño de Arquitectura de Backend: Diseñar e implementar una arquitectura de aplicación utilizando NestJS.
- Auth Process: Implementar la funcionalidad de autenticación y autorización utilizando JWT.
- Role Handling: Implementar manejo de roles y restricción de acceso basada en roles.
- Testing: Escribir pruebas unitarias para verificar el correcto funcionamiento de los endpoints, la lógica de negocio y la restricción de acceso.