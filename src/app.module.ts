/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppInit } from './app.init';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { ClientModule } from './client/client.module';
import { FilmModule } from './film/film.module';
import { Film } from './film/model/entity/film.entity';
import { Role } from './user/model/entity/role.entity';
import { User } from './user/model/entity/user.entity';
import { UsersModule } from './user/user.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: ':memory:',
      entities: [User, Role, Film],
      synchronize: true,
    }),    
    ConfigModule.forRoot(),
    AuthModule, 
    FilmModule,
    UsersModule,
    ClientModule
  ],
  controllers: [AppController],
  providers: [AppService, AppInit,],
})
export class AppModule {}
