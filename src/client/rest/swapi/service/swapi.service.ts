/* eslint-disable prettier/prettier */
import { HttpService } from "@nestjs/axios";
import {  Injectable, Logger } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";

@Injectable()
export class SwapiService {

  constructor(
    private readonly configService:ConfigService,
    private readonly httpService: HttpService
  ) {}

  fetchData() {
    const logger = new Logger();

    logger.log("Fetching data:" + this.configService.get<string>('SWAPI_BASE_URL'));
    return this.httpService.get(this.configService.get<string>('SWAPI_BASE_URL'));
  }

}
