import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { SwapiService } from './rest/swapi/service/swapi.service';
import { HttpModule } from '@nestjs/axios';
import axios from 'axios';

@Module({
  imports: [HttpModule, ConfigModule],
  providers: [
    SwapiService,
    {
      provide: 'AXIOS_INSTANCE', // Crea un proveedor para la instancia de Axios
      useValue: axios.create(), // Configura la instancia de Axios aquí según tus necesidades
    },
  ],
  exports: [SwapiService, 'AXIOS_INSTANCE'],
})
export class ClientModule {}
