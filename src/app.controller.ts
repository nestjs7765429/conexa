import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @ApiTags('Health')
  @ApiOkResponse({
    description: 'Return "ok" as response',
    type: String,
  })
  @Get('/health')
  getHealth(): string {
    return this.appService.getHealth();
  }
}
