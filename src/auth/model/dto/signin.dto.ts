/* eslint-disable prettier/prettier */
import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString, MinLength } from "class-validator";

export class SigninDTO {

    @ApiProperty({
        description: "The username",
        example: "admin"
    })
    @IsNotEmpty({ message: 'The username cant be null' })
    @IsString()
    username: string;

    @ApiProperty({
        description: "The password",
        example: "1234"
    })
    @MinLength(4)
    @IsNotEmpty({ message: 'The password cant be null' })
    @IsString()
    password: string;

}
