/* eslint-disable prettier/prettier */

import { ApiProperty } from "@nestjs/swagger";

export class JwtDTO {

    @ApiProperty({
        description: 'The access token',
        example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJhZG1pbiIsInJvbGVzIjpbeyJpZCI6MiwibmFtZSI6IkFETUlOSVNUUkFUT1IifV0sImlhdCI6MTY4OTgwMzAyNCwiZXhwIjoxNjg5ODg5NDI0fQ.b6Ox68chAWpTQY2Fc3AQaaNkU6UEH4lL1xUuk3ELlRg'
    })
    accessToken: string;

    setAccessToken(accessToken:string){
        this.accessToken = accessToken;
        return this;
    }
}
