/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

@Injectable()
export class PasswordEncrypterService {

    async encrypt(password): Promise<string>{
        //const salt = bcrypt.genSaltSync(10);
        const encriptedPassword = await bcrypt.hash(password, 10);
        return encriptedPassword;
    }

    async compare(password, passwordDB): Promise<boolean>{        
        return await bcrypt.compare(password, passwordDB);
    }
    
}
