/* eslint-disable prettier/prettier */
import { UserMapper } from './../../user/model/mapper/user.mapper';
import { SignupDTO } from '../model/dto/signup.dto';
import { JwtServicee } from './jwt.service';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserDTO } from 'src/user/model/dto/user.dto';
import { User } from 'src/user/model/entity/user.entity';
import { UserService } from 'src/user/service/user.service';
import { JwtDTO } from '../model/dto/jwt.dto';
import { SigninDTO } from '../model/dto/signin.dto';
import { PasswordEncrypterService } from '../security/util/password.encrypter.service';

@Injectable()
export class AuthService {

  constructor(
    private userService: UserService,
    private userMapper: UserMapper,
    private crypter: PasswordEncrypterService,
    private jwtService: JwtServicee,
  ) {}

  async signin(signinDTO:SigninDTO): Promise<JwtDTO> {
    
    const {username, password} =  signinDTO;

    const user = await this.userService.findByUsername(username);
    
    if (user === null || ! (await this.crypter.compare(password, user.password)) ) {
      throw new UnauthorizedException();
    }

    return this.jwtService.sign(user);

  }

  async signup(signupDTO:SignupDTO): Promise<UserDTO> {

    const user:User = await this.userMapper.toCreate(signupDTO)

    return this.userService.create(user).then((user) => {
        return this.userMapper.toDTO(user);
    });
    
  }

}