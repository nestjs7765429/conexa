import { JwtService } from '@nestjs/jwt';

/* eslint-disable prettier/prettier */
import { Injectable } from "@nestjs/common";
import { JwtDTO } from '../model/dto/jwt.dto';
import { User } from 'src/user/model/entity/user.entity';


@Injectable()
export class JwtServicee {

    constructor( private jwtService:JwtService){}

    sign(user:User):JwtDTO{
        const payload = {id: user.id, username: user.username, roles: user.roles};
        return new JwtDTO().setAccessToken(this.jwtService.sign(payload));
    }

}