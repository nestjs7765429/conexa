import { Module } from '@nestjs/common';
import { AuthController } from './controller/auth.controller';
import { AuthService } from './service/auth.service';
import { UsersModule } from 'src/user/user.module';
import { PasswordEncrypterService } from './security/util/password.encrypter.service';
import { ConfigModule } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { JwtServicee } from './service/jwt.service';
import { Constants } from './security/constant/constants';

@Module({
  imports: [
    UsersModule,
    ConfigModule,
    JwtModule.register({
      global: true,
      secret: Constants.JWT_SECRET,
      signOptions: { expiresIn: '1d' },
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, PasswordEncrypterService, JwtServicee],
  exports: [PasswordEncrypterService],
})
export class AuthModule {}
