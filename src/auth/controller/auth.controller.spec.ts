/* eslint-disable prettier/prettier */
// app.controller.spec.ts

import { HttpStatus, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from 'src/app.module';
import * as request from 'supertest';
import { SigninDTO } from '../model/dto/signin.dto';
import { SignupDTO } from '../model/dto/signup.dto';

describe('AuthController', () => {
  
  let app: INestApplication;

  let adminJwt;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    
    await app.init();
  
  }, 20000);

  afterAll(async () => {
    await app.close();
  });

  describe('POST /auth/signin', () => {
    
    it('should be signin', async () => {

      const signinDTO: SigninDTO = {username: 'admin', password: '1234'};

      const response = await request(app.getHttpServer())
        .post('/auth/signin')
        .send(signinDTO);

      adminJwt = response.body;

      expect(response.status).toBe(HttpStatus.OK);
      expect(response.body).toBeInstanceOf(Object);
      expect(response.body.accessToken).toBeDefined();
    });

    
    it('should not be signin', async () => {
      
      const signinDTO: SigninDTO = {username: 'inexistente', password: 'sin-password'};

      const response = await request(app.getHttpServer())
        .post('/auth/signin')
        .send(signinDTO);

      expect(response.status).toBe(HttpStatus.UNAUTHORIZED);

    });

    it('should create a user', async () => {
      
      const signupDTO: SignupDTO = {username: 'Daro', password: '1234'};

      const response = await request(app.getHttpServer())
        .post('/auth/signup')
        .set('Authorization', `Bearer ${adminJwt.accessToken}`)
        .send(signupDTO);

      expect(response.status).toBe(HttpStatus.CREATED);

    });

    it('should login with daro', async () => {
      
      const signinDTO: SigninDTO = {username: 'Daro', password: '1234'};

      const response = await request(app.getHttpServer())
        .post('/auth/signin')
        .send(signinDTO);

      expect(response.status).toBe(HttpStatus.OK);
      expect(response.body).toBeInstanceOf(Object);
      expect(response.body.accessToken).toBeDefined();

    });
    
  });
});
