/* eslint-disable prettier/prettier */
import { Body, Controller, HttpCode, HttpStatus, Post, UseGuards } from '@nestjs/common';
import { AuthService } from '../service/auth.service';
import { SigninDTO } from '../model/dto/signin.dto';
import { SignupDTO } from '../model/dto/signup.dto';
import { ApiBearerAuth, ApiOkResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { JwtGuard } from '../security/guard/jwt.guard';
import { JwtDTO } from '../model/dto/jwt.dto';
import { Roles } from '../security/decorator/role.decorator';
import { RoleEnum } from '../model/enum/role.enum';
import { RolesGuard } from '../security/guard/role.guard';
import { UserDTO } from 'src/user/model/dto/user.dto';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
    
    constructor(private readonly authService: AuthService) {}
    
    @ApiOkResponse({
        description: 'Return de jwt access token',
        type: JwtDTO,
    })
    @ApiUnauthorizedResponse({
        description: 'Unauthorized',
    })
    @HttpCode(HttpStatus.OK)
    @Post('/signin')
    async signin(@Body() signinDTO: SigninDTO): Promise<JwtDTO> {
        return await this.authService.signin(signinDTO);
    }

    @ApiOkResponse({
        description: 'Return de user created',
        type: UserDTO,
    })
    @ApiUnauthorizedResponse({
        description: 'Unauthorized',
    })
    @ApiBearerAuth()
    @UseGuards(JwtGuard, RolesGuard)
    @Roles(RoleEnum.Admin)
    @HttpCode(HttpStatus.CREATED)
    @Post('/signup')
    async signup(@Body() signupDTO: SignupDTO) {
        return await this.authService.signup(signupDTO);
    }

}
