import { SwapiService } from './client/rest/swapi/service/swapi.service';
/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-empty-function */
import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { RoleEnum } from './auth/model/enum/role.enum';
import { PasswordEncrypterService } from './auth/security/util/password.encrypter.service';
import { Role } from './user/model/entity/role.entity';
import { User } from './user/model/entity/user.entity';
import { RoleService } from './user/service/role.service';
import { UserService } from './user/service/user.service';
import { FilmService } from './film/service/film.service';
import { FilmMapper } from './film/model/mapper/film.mapper';
import { FilmDTO } from './film/model/dto/film.dto';
import { firstValueFrom } from 'rxjs';

@Injectable() 
export class AppInit implements OnModuleInit{

  logger = new Logger();

  constructor(    
    private readonly userService: UserService,
    private readonly roleService: RoleService,
    private readonly configService: ConfigService,
    private readonly crypter: PasswordEncrypterService,
    private readonly swapiService: SwapiService,
    private readonly filmService: FilmService,
    private readonly filmMapper: FilmMapper,
        
  ) {}
  
  async onModuleInit() {
    await this.init();
  }

  async init() {

    try{
    
    this.logger.log('Inizializing AppInit.. please wait', 'AppInit');
    
    let roleAdministrator = new Role();
    roleAdministrator.name = RoleEnum.Admin;
    roleAdministrator = await this.roleService.create(roleAdministrator);

    this.logger.log('Inizializing.. Adding admin role', 'AppInit');
    
    let roleUser = new Role();    
    roleUser.name = RoleEnum.User;
    roleUser = await this.roleService.create(roleUser);

    this.logger.log('Inizializing.. Adding user role', 'AppInit');
  
    const admin = new User();
    admin.username = this.configService.get<string>('ADMIN_USER');
    admin.password = await this.crypter.encrypt(this.configService.get<string>('ADMIN_PASSWORD'));
    admin.roles = [roleAdministrator];
    await this.userService.create(admin);

    this.logger.log('Inizializing.. Adding admin', 'AppInit');
    
    const response = await firstValueFrom(this.swapiService.fetchData());

    response.data?.results?.map(dataFilm => {          
      const film:FilmDTO = this.filmMapper.toImport(dataFilm);      
      this.filmService.import(film);
    }) 

    this.logger.log('Inizializing.. Search api data', 'AppInit');
    
    
    this.logger.log('Inizializing Complete..', 'AppInit');
  }catch(e){
    this.logger.log(e);
  }
  
  }

}
