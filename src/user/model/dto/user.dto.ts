/* eslint-disable prettier/prettier */
import { Expose } from 'class-transformer';
import { RoleDTO } from './role.dto';
import { ApiProperty } from '@nestjs/swagger';

export class UserDTO {
  @ApiProperty({
    description: 'The user id',
    example: '1',
  })
  @Expose()
  id: number;

  @ApiProperty({
    description: 'The username',
    example: 'darpep'
  })
  @Expose()
  username: string;
  @ApiProperty({
    description: 'The collections of roles',
    type: [RoleDTO],
  })
  @Expose()
  roles: RoleDTO[];
}
