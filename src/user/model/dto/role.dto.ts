/* eslint-disable prettier/prettier */
import { ApiProperty } from "@nestjs/swagger";
import { Expose } from "class-transformer";

export class RoleDTO {
  @ApiProperty({
    description: 'The role id ',
    example: 1
  })
  @Expose()
  id: number;
  @ApiProperty({
    description: 'The role name',
    example: 'ADMIN'
  })
  @Expose()
  name: string;
}
