/* eslint-disable prettier/prettier */
import { Injectable } from "@nestjs/common";
import { plainToClass } from "class-transformer";
import { RoleDTO } from "../dto/role.dto";
import { Role } from "../entity/role.entity";

@Injectable()
export class RoleMapper {

  toDTO(roleDTO: Role): RoleDTO {
    return plainToClass(RoleDTO, roleDTO, { excludeExtraneousValues: true });
  }

  toEntity(roleDTO: RoleDTO): Role {
    return plainToClass(Role, roleDTO, { excludeExtraneousValues: true });
  }

}