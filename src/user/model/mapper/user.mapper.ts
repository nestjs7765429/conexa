/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { SignupDTO } from 'src/auth/model/dto/signup.dto';
import { RoleService } from '../../service/role.service';
import { UserDTO } from '../dto/user.dto';
import { User } from '../entity/user.entity';
import { RoleMapper } from './role.mapper';
import { PasswordEncrypterService } from 'src/auth/security/util/password.encrypter.service';
import { RoleEnum } from 'src/auth/model/enum/role.enum';



@Injectable()
export class UserMapper {

  constructor(
    private roleMapper: RoleMapper,
    private roleService: RoleService,
    private crypter: PasswordEncrypterService,
  ) { 
    
  }

  toDTO(user: User): UserDTO {
    const userDTO:UserDTO = plainToClass(UserDTO, user, { excludeExtraneousValues: true });
    userDTO.roles = user.roles.map(role => {      
      return this.roleMapper.toDTO(role);    
    })
    return userDTO;
  }

  toEntity(userDTO: UserDTO): User {
    const user:User = plainToClass(User, userDTO, { excludeExtraneousValues: true });
    user.roles = userDTO.roles.map(roleDTO => {      
      return this.roleMapper.toEntity(roleDTO);    
    })
    return user;
  }

  async toCreate(signupDTO:SignupDTO): Promise<User>{    

    const user:User = new User()
      .setUsername(signupDTO.username)
      .setPassword(await this.crypter.encrypt(signupDTO.password));

    user.roles = [await this.roleService.findByName(RoleEnum.User)];

    return user;
  };

}