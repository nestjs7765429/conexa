/* eslint-disable prettier/prettier */
import { Entity, Column, PrimaryGeneratedColumn, OneToMany, Index } from 'typeorm';
import { Role } from './role.entity';
import { Exclude } from 'class-transformer';
@Entity()
export class User {
  
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Index({ unique: true })
  username: string;

  @Column()
  password: string;

  @Exclude()
  @OneToMany(() => Role, role => role.user)
  roles: Role[];

  setUsername(username:string){
    this.username = username;
    return this;
  }

  setPassword(password:string){
    this.password = password;
    return this;
  }

  setRoles(roles?:Role[]){
    this.roles = roles;
    return this;
  }
}
