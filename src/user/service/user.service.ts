/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { User } from '../model/entity/user.entity';
import { UserRepository } from '../repository/user.repository';

@Injectable()
export class UserService {

  constructor(private readonly userRepository: UserRepository) {}

  async findByUsername(username: string): Promise<User | undefined> {
    return await this.userRepository.findByUsername(username);
  }  

  create(user: User): Promise<User | undefined> {
    return this.userRepository.create(user);
  }
  
}
