/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { Role } from '../model/entity/role.entity';
import { RoleRepository } from '../repository/role.repository';

@Injectable()
export class RoleService {

  constructor(private readonly roleRepository: RoleRepository) {}

  findByName(name: string): Promise<Role | undefined> {
    return this.roleRepository.findByName(name);
  }

  create(role:Role): Promise<Role | undefined> {
    return this.roleRepository.create(role);
  }
  
}
