/* eslint-disable prettier/prettier */
// user.service.ts
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../model/entity/user.entity';


@Injectable()
export class UserRepository {
  
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  findAll(): Promise<User[]> {
    return this.userRepository.find();
  }

  findByUsername(username:string): Promise<User | null> {
    return this.userRepository.findOne({ where: { username }, relations: ['roles'] });
  }

  async create(user: Partial<User>): Promise<User> {
    const newUser = this.userRepository.create({
        ...user,
        id: null,
    });

    try {
        return await this.userRepository.save(newUser);    
    } catch (error) {
        throw new HttpException({
            status: HttpStatus.BAD_REQUEST,
            error: 'Allready exists: ' + newUser.username,
        }, HttpStatus.BAD_REQUEST);
    }
    
  }

}
