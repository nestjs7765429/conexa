/* eslint-disable prettier/prettier */
// user.service.ts
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Role } from '../model/entity/role.entity';


@Injectable()
export class RoleRepository {
  
  constructor(
    @InjectRepository(Role)
    private readonly roleRepository: Repository<Role>,
  ) {}

  findByName(name:string): Promise<Role | null> {
    return this.roleRepository.findOneBy({name:name});
  }

  async create(role: Partial<Role>): Promise<Role> {
    const newRole = this.roleRepository.create({
        ...role,
        id: null,
    });

    try {
        return await this.roleRepository.save(newRole);    
    } catch (error) {
        throw new HttpException({
            status: HttpStatus.CONFLICT,
            error: error?.driverError
        }, HttpStatus.CONFLICT);
    }
    
  }

}
