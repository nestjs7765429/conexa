import { Module } from '@nestjs/common';
import { RoleMapper } from './model/mapper/role.mapper';
import { UserMapper } from './model/mapper/user.mapper';
import { RoleRepository } from './repository/role.repository';
import { UserRepository } from './repository/user.repository';
import { RoleService } from './service/role.service';
import { UserService } from './service/user.service';
import { ConfigService } from '@nestjs/config';
import { PasswordEncrypterService } from 'src/auth/security/util/password.encrypter.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './model/entity/user.entity';
import { Role } from './model/entity/role.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, Role])],
  providers: [
    UserService,
    UserRepository,
    RoleService,
    RoleRepository,
    UserMapper,
    RoleMapper,
    PasswordEncrypterService,
    ConfigService,
  ],
  exports: [
    UserService,
    RoleService,
    UserMapper,
    RoleMapper,
    UserRepository,
    RoleRepository,
  ],
})
export class UsersModule {}
