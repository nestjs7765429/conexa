/* eslint-disable prettier/prettier */
import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
@Entity()
export class Film {
  
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    episode_id: number;

    @Column({ nullable: true })
    opening_crawl: string;

    @Column({ nullable: true })
    director: string;

    @Column({ nullable: true })
    producer: string

    @Column({ nullable: true })
    release_date: Date;

    @Column('json', { nullable: true })
    characters: object;

    @Column('json', { nullable: true })
    planets: object;

    @Column('json', { nullable: true })
    starships: object;

    @Column('json', { nullable: true })
    vehicles: object;

    @Column('json', { nullable: true })
    species: object;

    @Column()
    @CreateDateColumn()
    created: Date;

    @Column({ nullable: true })
    @UpdateDateColumn()
    edited: Date;

    @Column({ nullable: true })
    url: string;

}
