/* eslint-disable prettier/prettier */
import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsDate, IsDateString, IsJSON, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class FilmDTO {

    @ApiProperty({
        description: 'The film id',
        example: 1,
        readOnly: true
    })
    @IsOptional()
    @IsNumber()
    id: number;

    @ApiProperty({
        description: 'The film title',
        example: 'The Empire Strikes Back'
    })
    @IsNotEmpty({ message: 'The title can not be null' })
    @IsString()
    title: string;

    @ApiProperty({
        description: 'The film episode number',
        example: 5
    })
    @IsNotEmpty({ message: 'The episode can not be null' })
    @IsNumber()
    episode_id: number;

    @ApiProperty({
        description: 'The film summary',
        example: 'It is a dark time for the\r\nRebellion. Although the Death\r\nStar has been destroyed...',
        required: false
    })
    @IsOptional()
    @IsString()
    opening_crawl: string;

    @ApiProperty({
        description: 'The film director',
        example: 'Irvin Kershner',
        required: false
    })
    @IsOptional()
    @IsString()
    director: string;

    @ApiProperty({
        description: 'The film producer',
        example: 'Gary Kurtz, Rick McCallum',
        required: false
    })
    @IsOptional()
    @IsString()
    producer: string

    @ApiProperty({
        description: 'The film release date',
        example: '1980-05-17T00:00:00.000Z',
        required: false
    })
    @IsOptional()
    @IsDateString()
    release_date: Date;
    
    @ApiProperty({
        description: 'The film characters resources as objects',
        example: '["https://swapi.dev/api/people/1/","https://swapi.dev/api/people/2/"]',
        type: '[String]',
        required: false
    })
    @IsOptional()
    characters: object;

    @ApiProperty({
        description: 'The film planets resources as objects',
        example: '["https://swapi.dev/api/planets/1/","https://swapi.dev/api/planets/2/"]',
        type: '[String]',
        required: false
    })
    @IsOptional()
    planets: object;

    @ApiProperty({
        description: 'The film characters resources as objects',
        example: '["https://swapi.dev/api/starships/1/","https://swapi.dev/api/starships/2/"]',
        type: '[String]',
        required: false
    })
    @IsOptional()
    starships: object;

    @ApiProperty({
        description: 'The film characters resources as objects',
        example: '["https://swapi.dev/api/vehicles/1/","https://swapi.dev/api/vehicles/2/"]',
        type: '[String]',
        required: false
    })
    @IsOptional()
    vehicles: object;

    @ApiProperty({
        description: 'The film characters resources as objects',
        example: '["https://swapi.dev/api/species/1/","https://swapi.dev/api/species/2/"]',
        type: '[String]',
        required: false
    })
    @IsOptional()
    species: object;

    @ApiProperty({
        description: 'The registry created date',
        example: '2014-12-19T16:52:55.740Z',
        readOnly: true
    })
    @IsOptional()
    @IsDateString()
    created: Date;

    @ApiProperty({
        description: 'The registry updated date',
        example: '2014-12-19T16:52:55.740Z',
        readOnly: true
    })
    @IsOptional()
    @IsDateString()
    edited: Date;

    @ApiProperty({
        description: 'The film url resource',
        example: 'https://localhost:3000/api/films/4/',
        readOnly: true
    })
    @IsOptional()
    @IsString()
    url: string;

}
