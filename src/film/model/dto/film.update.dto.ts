import { Exclude } from 'class-transformer';
/* eslint-disable prettier/prettier */
import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';
import { FilmDTO } from './film.dto';

export class FilmUpdateDTO extends PartialType(FilmDTO){

    @ApiProperty({
        readOnly:true
    })
    @Exclude()    
    id: number;

    @ApiProperty({
        description: 'The film title',
        example: 'The Empire Strikes Back'
    })
    @IsOptional()
    title: string;

    @ApiProperty({
        description: 'The film episode number',
        example: 5
    })
    @IsOptional()
    episode_id: number;

}
