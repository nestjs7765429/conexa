/* eslint-disable prettier/prettier */
import { Injectable } from "@nestjs/common";
import { plainToClass } from "class-transformer";
import { FilmDTO } from "../dto/film.dto";
import { Film } from "../entity/film.entity";
import { FilmUpdateDTO } from "../dto/film.update.dto";
import { Request } from 'express';
@Injectable()
export class FilmMapper {
  
  toEntity(filmDTO:FilmDTO): Film{
    
    return plainToClass(Film, filmDTO);
    
  };

  toDTO(film:Film): FilmDTO{
    
    return plainToClass(FilmDTO, film);
    
  };

  toMerge(film: Film ,filmUpdateDTO: FilmUpdateDTO): Film{

    //para que aplique las restricciones del FilmUpdateDTO
    const updatedFilm = plainToClass(Film, filmUpdateDTO);
    
    return Object.assign(film, updatedFilm);
    
  };

  setUrl(film: Film, request: Request): Film {

    const baseUrl = request.protocol + '://' + request.get('host') + request.originalUrl;

    film.url = `${baseUrl}/${film.id}`;

    return film;

  }

  toImport(dataFilm:any): Film{
    
    const film:Film = dataFilm;

    return film;
    
  };

}