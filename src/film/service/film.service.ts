import { FilmMapper } from '../model/mapper/film.mapper';
/* eslint-disable prettier/prettier */
import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { FilmDTO } from '../model/dto/film.dto';
import { Film } from '../model/entity/film.entity';
import { FilmRepository } from '../repository/film.repository';
import { FilmUpdateDTO } from '../model/dto/film.update.dto';
import { Request } from 'express';

@Injectable()
export class FilmService {

  constructor(
    private readonly filmRepository: FilmRepository,
    private readonly filmMapper:FilmMapper,
    ) {}

  async findAll(): Promise<FilmDTO[]>{

    const films:Film[] = await this.filmRepository.findAll();

    if(!films.length)
      throw new HttpException('No Content' , HttpStatus.NO_CONTENT);
    
    return films.map(film => this.filmMapper.toDTO(film));

  }

  async findById(id:number): Promise<FilmDTO>{

    const film:Film = await this.filmRepository.findById(id);

    if(!film)
      throw new NotFoundException();
    
    return this.filmMapper.toDTO(film);

  }

  async create(filmDTO: FilmDTO, request: Request): Promise<FilmDTO> {

    let film:Film = this.filmMapper.toEntity(filmDTO);

    film = await this.filmRepository.create(film);

    film = this.filmMapper.setUrl(film, request);

    film = await this.filmRepository.update(film);

    return this.filmMapper.toDTO(film);

  }

  async update(id: number, filmUpdateDTO: FilmUpdateDTO): Promise<FilmDTO> {

    let film:Film = await this.filmRepository.findById(id);

    if(!film)
      throw new NotFoundException();

    film = this.filmMapper.toMerge(film, filmUpdateDTO);

    film = await this.filmRepository.update(film);

    return this.filmMapper.toDTO(film);
  }

  async delete(id: number): Promise<void> {

    const film:Film = await this.filmRepository.findById(id);

    if(!film)
      throw new NotFoundException();

    await this.filmRepository.delete(id);

  }

  async import(filmDTO: FilmDTO): Promise<void> {

    const film:Film = this.filmMapper.toEntity(filmDTO);

    await this.filmRepository.create(film);

  }
  
}