/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable prettier/prettier */
import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, ParseIntPipe, Patch, Post, Req, UseGuards } from '@nestjs/common';
import { ApiAcceptedResponse, ApiBadRequestResponse, ApiBearerAuth, ApiCreatedResponse, ApiNoContentResponse, ApiNotFoundResponse, ApiOkResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { JwtGuard } from 'src/auth/security/guard/jwt.guard';
import { FilmService } from '../service/film.service';
import { FilmDTO } from '../model/dto/film.dto';
import { RolesGuard } from 'src/auth/security/guard/role.guard';
import { Roles } from 'src/auth/security/decorator/role.decorator';
import { RoleEnum } from 'src/auth/model/enum/role.enum';
import { FilmUpdateDTO } from '../model/dto/film.update.dto';
import { Request } from 'express';

@ApiBearerAuth()
@ApiTags('Films')
@Controller('films')
export class FilmController {

    constructor(private filmService: FilmService) {}
    
    @ApiOkResponse({
        description: 'A list of films',
        type: [FilmDTO],
    })
    @ApiNoContentResponse({
        description: 'No Content',
    })
    @ApiUnauthorizedResponse({
        description: 'Unauthorized',
    })
    @UseGuards(JwtGuard)
    @HttpCode(HttpStatus.OK)
    @Get()
    async findAll(): Promise<FilmDTO[]>{
        return await this.filmService.findAll();
    }

    @ApiOkResponse({
        description: 'A film',
        type: FilmDTO,
    })
    @ApiNotFoundResponse({
        description: 'Not Found',
    })
    @ApiUnauthorizedResponse({
        description: 'Unauthorized',
    })
    @ApiBadRequestResponse({
        description: 'Bad Request',
    })
    @UseGuards(JwtGuard, RolesGuard)
    @Roles(RoleEnum.User)
    @Get('/:id')
    async findById(@Param('id', ParseIntPipe)id:number): Promise<FilmDTO>{
        return await this.filmService.findById(id);
    }


    @ApiCreatedResponse({
        description: 'Created',
        type: FilmDTO,
    })
    @ApiUnauthorizedResponse({
        description: 'Unauthorized',
    })
    @ApiBadRequestResponse({
        description: 'Bad Request',
    })
    @UseGuards(JwtGuard, RolesGuard)
    @Roles(RoleEnum.Admin)
    @HttpCode(HttpStatus.CREATED)
    @Post()
    async create(@Body() filmDTO: FilmDTO, @Req() request: Request): Promise<FilmDTO>{        
        return await this.filmService.create(filmDTO, request);
    }

    @ApiAcceptedResponse({
        description: 'Acepted',
        type: FilmDTO,
    })
    @ApiNotFoundResponse({
        description: 'Not Found',
    })
    @ApiUnauthorizedResponse({
        description: 'Unauthorized',
    })
    @ApiBadRequestResponse({
        description: 'Bad Request',
    })
    @UseGuards(JwtGuard, RolesGuard)
    @Roles(RoleEnum.Admin)
    @HttpCode(HttpStatus.ACCEPTED)
    @Patch(':id')
    async update(@Param('id', ParseIntPipe)id:number, @Body() filmUpdateDTO: FilmUpdateDTO ): Promise<FilmDTO>{
        return await this.filmService.update(id, filmUpdateDTO);
    }

    @ApiAcceptedResponse({
        description: 'Acepted'
    })
    @ApiNotFoundResponse({
        description: 'Not Found',
    })
    @ApiUnauthorizedResponse({
        description: 'Unauthorized',
    })
    @ApiBadRequestResponse({
        description: 'Bad Request',
    })
    @HttpCode(HttpStatus.ACCEPTED)
    @UseGuards(JwtGuard, RolesGuard)
    @Roles(RoleEnum.Admin)
    @Delete(':id')
    async delete(@Param('id', ParseIntPipe)id:number): Promise<void>{
        await this.filmService.delete(id);
    }
    
}