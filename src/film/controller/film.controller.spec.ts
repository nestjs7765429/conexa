/* eslint-disable prettier/prettier */
// app.controller.spec.ts

import { HttpStatus, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from 'src/app.module';
import * as request from 'supertest';
import { SigninDTO } from '../../auth/model/dto/signin.dto';

describe('FilmController', () => {
  let app: INestApplication;

  let jwt;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    
    await app.init();

  }, 20000);

  afterAll(async () => {
    await app.close();
  });

  describe('Resource: /films', () => {

    it('should be signin', async () => {

      const signinDTO: SigninDTO = {username: 'admin', password: '1234'};

      const response = await request(app.getHttpServer())
      .post('/auth/signin')
      .send(signinDTO);

      jwt = response.body;

    });

    it('should be a list of films', async () => {      

      const response = await request(app.getHttpServer())
        .get('/films')
        .set('Authorization', `Bearer ${jwt.accessToken}`)

      expect(response.status).toBe(HttpStatus.OK);
      expect(response.body).toBeInstanceOf(Object);

    });

    
    it('should not get a film for admin', async () => {            

      const response = await request(app.getHttpServer())
        .get('/films/1')
        .set('Authorization', `Bearer ${jwt.accessToken}`);

      expect(response.status).toBe(HttpStatus.FORBIDDEN);

    });

    it('should create a film', async () => {    
      
      const filmDTO = {title: 'The Empire Strikes Back', episode_id: 7};

      const response = await request(app.getHttpServer())
        .post('/films')
        .set('Authorization', `Bearer ${jwt.accessToken}`)
        .send(filmDTO);

      expect(response.status).toBe(HttpStatus.CREATED);

      expect(response.body.title).toBe('The Empire Strikes Back');

    });

    it('should update a film', async () => {    
      
      const filmDTO = {title: 'Darth Vader', opening_crawl: 'It is a dark time for the\r\nRebellion. Although the Death\r\nStar has been destroyed...'};

      const response = await request(app.getHttpServer())
        .patch('/films/7')
        .set('Authorization', `Bearer ${jwt.accessToken}`)
        .send(filmDTO);

      expect(response.status).toBe(HttpStatus.ACCEPTED);

      expect(response.body.title).toBe('Darth Vader');

    });

    it('should not found on update a film', async () => {    
      
      const filmDTO = {title: 'Darth Vader', opening_crawl: 'It is a dark time for the\r\nRebellion. Although the Death\r\nStar has been destroyed...'};

      const response = await request(app.getHttpServer())
        .patch('/films/18')
        .set('Authorization', `Bearer ${jwt.accessToken}`)
        .send(filmDTO);

      expect(response.status).toBe(HttpStatus.NOT_FOUND);

    });

    it('should delete a film', async () => {    

      const response = await request(app.getHttpServer())
        .delete('/films/1')
        .set('Authorization', `Bearer ${jwt.accessToken}`)

      expect(response.status).toBe(HttpStatus.ACCEPTED);

    });
    
  });
  
});
