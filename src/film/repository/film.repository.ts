/* eslint-disable prettier/prettier */
// user.service.ts
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Film } from '../model/entity/film.entity';


@Injectable()
export class FilmRepository {
  
  constructor(
    @InjectRepository(Film)
    private readonly filmRepository: Repository<Film>,
  ) {}

  async findAll(): Promise<Film[]> {
    return await this.filmRepository.find()
  }

  async findById(id:number): Promise<Film> {
    return await this.filmRepository.findOne({where:{id}});
  }

 async create(film: Partial<Film>): Promise<Film> {
    
    const newFilm = this.filmRepository.create({
        ...film,
        id: null
    });
    
    try {
        return await this.filmRepository.save(newFilm);
    } catch (error) {        
        throw new HttpException({
            status: HttpStatus.INTERNAL_SERVER_ERROR,
            error: 'An errors ocurred creating: ' + film.title,
        }, HttpStatus.BAD_REQUEST);
    }
    
  }

  async update(film: Partial<Film>): Promise<Film> {

    try {
        return await this.filmRepository.save(film);    
    } catch (error) {
        throw new HttpException({
            status: HttpStatus.INTERNAL_SERVER_ERROR,
            error: 'An errors ocurred updating: ' + film.title,
        }, HttpStatus.BAD_REQUEST);
    }
    
  }

  async delete(id:number): Promise<void> {

    try {
        await this.filmRepository.delete(id);    
    } catch (error) {
        throw new HttpException({
            status: HttpStatus.INTERNAL_SERVER_ERROR,
            error: 'An errors ocurred deleting id: ' + id,
        }, HttpStatus.BAD_REQUEST);
    }
    
  }

}
