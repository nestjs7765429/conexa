import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { FilmService } from './service/film.service';
import { FilmRepository } from './repository/film.repository';
import { FilmMapper } from './model/mapper/film.mapper';
import { FilmController } from './controller/film.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Film } from './model/entity/film.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Film]), ConfigModule],
  controllers: [FilmController],
  providers: [FilmService, FilmRepository, FilmMapper],
  exports: [FilmService, FilmMapper],
})
export class FilmModule {}
